package br.com.digio.androidtest

data class Cash(
    val bannerURL: String,
    val title: String
)