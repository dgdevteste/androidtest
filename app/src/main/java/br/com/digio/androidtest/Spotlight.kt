package br.com.digio.androidtest

data class Spotlight(
    val bannerURL: String,
    val name: String,
    val description: String
)